openapi: "3.0.3"
externalDocs:
  url: https://gitlab.utwente.nl/claudenirmf/mod4-wp-2023-2024-pokemon
info:
  title: PokeApp API
  version: "1.0"
  license:
    name: Creative Commons Attribution 4.0 International (CC-BY-4.0)
    url: https://creativecommons.org/licenses/by/4.0/deed.en
  description: The [PokeApp API](https://gitlab.utwente.nl/claudenirmf/mod4-wp-2023-2024-pokemon) is a REST API part of a sample web application designed to teach web development concepts at the [Data & Information module](https://www.utwente.nl/en/education/bachelor/programmes/technical-computer-science/study-programme/#modules-technical-computer-science) of the [Technical Computer Science](https://www.utwente.nl/en/education/bachelor/programmes/technical-computer-science/) program of the [University of Twente](https://utwente.nl/en).
  contact:
    name: Claudenir M. Fonseca
    url: https://gitlab.utwente.nl/claudenirmf/mod4-wp-2023-2024-pokemon/issues
    email: c.moraisfonseca@utwente.nl
servers:
  - description: Local Deployment
    url: http://localhost:8080/pokemon/api
tags:
  - name: Trainer
    description: Routes for managing data about trainers.
  - name: Pokémon
    description: Routes for managing data about pokémon.
#  - name: Pokémon Type
#    description: Routes for managing data about pokémon types.
paths:
  /trainers:
    get:
      description: Retrieves a collection of resources.
      tags:
        - Trainer
      parameters:
        - name: pageNumber
          description: Returns the desired page.
          in: query
          schema:
            type: number
        - name: pageSize
          description: Returns the desired number of resources.
          in: query
          schema:
            type: number
      responses:
        200:
          description: OK
    post:
      description: Creates a resource on the route's collection.
      tags:
        - Trainer
      requestBody:
        content:
          'application/json':
            schema:
              type: object
      responses:
        200:
          description: OK
        400:
          description: Bad request
        405:
          description: Server error
  /trainers/{id}:
    get:
      description: Retrieves a resource with matching a {id}.
      tags:
        - Trainer
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        200:
          description: OK
        404:
          description: Not found
    put:
      description: Replaces the resource {id} with the provided one.
      tags:
        - Trainer
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      
      requestBody:
        content:
          'application/json':
            schema:
              type: object
      responses:
        200:
          description: OK
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error
    delete:
      description: Deletes the resource identified as {id}.
      tags:
        - Trainer
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        204:
          description: No content
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error

#  /pokemon:
#    get:
#      description: Retrieves a collection of resources.
#      tags:
#        - Pokémon
#      responses:
#        200:
#          description: OK
#    post:
#      description: Creates a resource on the route's collection.
#      tags:
#        - Pokémon
#      requestBody:
#        content:
#          'application/json':
#            schema:
#              type: object
#      responses:
#        200:
#          description: OK
#        400:
#          description: Bad request
#        405:
#          description: Server error
#  /pokemon/{id}:
#    get:
#      description: Retrieves a resource with matching a {id}.
#      tags:
#        - Pokémon
#      parameters:
#        - name: id
#          description: The {id} of the desired resource.
#          in: path
#          schema:
#            type: string
#            minLength: 1
#          required: true
#      responses:
#        200:
#          description: OK
#        404:
#          description: Not found
#    put:
#      description: Replaces the resource {id} with the provided one.
#      tags:
#        - Pokémon
#      parameters:
#        - name: id
#          description: The {id} of the desired resource.
#          in: path
#          schema:
#            type: string
#            minLength: 1
#          required: true
#
#      requestBody:
#        content:
#          'application/json':
#            schema:
#              type: object
#      responses:
#        200:
#          description: OK
#        400:
#          description: Bad request
#        404:
#          description: Not found
#        405:
#          description: Server error
#    delete:
#      description: Deletes the resource identified as {id}.
#      tags:
#        - Pokémon
#      parameters:
#        - name: id
#          description: The {id} of the desired resource.
#          in: path
#          schema:
#            type: string
#            minLength: 1
#          required: true
#      responses:
#        204:
#          description: No content
#        400:
#          description: Bad request
#        404:
#          description: Not found
#        405:
#          description: Server error

  /pokemonTypes:
    get:
      description: Retrieves a collection of resources.
      tags:
        - Pokémon Type
      parameters:
        - name: pageNumber
          description: Returns the desired page.
          in: query
          schema:
            type: number
        - name: pageSize
          description: Returns the desired number of resources.
          in: query
          schema:
            type: number
        - name: sortBy
          description: Returns resources sorted according the desired attribute.
          in: query
          schema:
            type: string
            enum:
              - id
              - pokedexNumber
              - lastUpDate
      responses:
        200:
          description: OK
    post:
      description: Creates a resource on the route's collection.
      tags:
        - Pokémon Type
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/Trainer'
      responses:
        200:
          description: OK
        400:
          description: Bad request
        405:
          description: Server error
  /pokemonTypes/{id}:
    get:
      description: Retrieves a resource with matching a {id}.
      tags:
        - Pokémon Type
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        200:
          description: OK
        404:
          description: Not found
    put:
      description: Replaces the resource {id} with the provided one.
      tags:
        - Pokémon Type
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      
      requestBody:
        content:
          'application/json':
            schema:
              type: object
      responses:
        200:
          description: OK
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error
    delete:
      description: Deletes the resource identified as {id}.
      tags:
        - Pokémon Type
      parameters:
        - name: id
          description: The {id} of the desired resource.
          in: path
          schema:
            type: string
            minLength: 1
          required: true
      responses:
        204:
          description: No content
        400:
          description: Bad request
        404:
          description: Not found
        405:
          description: Server error

components:
  schemas:
    Trainer:
      description: Schema describing the overall shape of trainer resources.
      type: object
      properties:
        id:
          type: string
          minLength: 1
        created:
          type: string
          format: date-time
        lastUpdate:
          type: string
          format: date-time
        name:
          type: string
          minLength: 1
        profileUrl:
          type: string
          format: uri
        pokemon:
          type: array
          items:
            type: string
            minLength: 1
        party:
          type: array
          items:
            type: string
            minLength: 1

#    Pokemon:
#      description: Schema describing the overall shape of pokemon resources.
#      type: object
#      properties:
#        id:
#          type: string
#          minLength: 1
#        created:
#          type: string
#          format: date-time
#        lastUpdate:
#          type: string
#          format: date-time
#        name:
#          type: string
#          minLength: 1
#        height:
#          type: number
#        weight:
#          type: number
#        hp:
#          type: number
#        attack:
#          type: number
#        spAttack:
#          type: number
#        defense:
#          type: number
#        spDefense:
#          type: number
#        speed:
#          type: number
#        pokemonType:
#          type: string
#          minLength: 1

    PokemonType:
      description: Schema describing the overall shape of pokemon type resources.
      type: object
      properties:
        id:
          type: string
          minLength: 1
        created:
          type: string
          format: date-time
        lastUpdate:
          type: string
          format: date-time
        name:
          type: string
          minLength: 1
        pokedexNumber:
          type: number
          minimum: 0
        generation:
          type: number
          minimum: 0
        japaneseName:
          type: string
        classification:
          type: string
        abilities:
          type: array
          items:
            type: string
            minLength: 1
        baseHeight:
          type: number
          minimum: 0
        baseWeight:
          type: number
          minimum: 0
        baseHp:
          type: number
          minimum: 0
        baseAttack:
          type: number
          minimum: 0
        baseSpAttack:
          type: number
          minimum: 0
        baseDefense:
          type: number
          minimum: 0
        baseSpDefense:
          type: number
          minimum: 0
        baseSpeed:
          type: number
          minimum: 0
        captureRate:
          type: number
          minimum: 0
        isLegendary:
          type: boolean
        imgUrl:
          type: string
          format: uri
        primaryType:
          type: string
          enum:
            - bug
            - dark
            - dragon
            - electric
            - fairy
            - fighting
            - fire
            - flying
            - ghost
            - grass
            - ground
            - ice
            - normal
            - poison
            - psychic
            - rock
            - steel
            - water
        secondaryType:
          type: string
          enum:
            - bug
            - dark
            - dragon
            - electric
            - fairy
            - fighting
            - fire
            - flying
            - ghost
            - grass
            - ground
            - ice
            - normal
            - poison
            - psychic
            - rock
            - steel
            - water
